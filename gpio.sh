#!/usr/bin/env bash

GPIO_PATH=/sys/class/gpio
GPIO_META_PATH=/pantavisor/user-meta/gpio

log() {
	if [ $$ == 1 ] || [ ! -e /var/log/messages ]; then
		echo $1 > /dev/console
	else
		logger $1
	fi
}

set_gpio_value() {
	if [ ! -e $GPIO_PATH/gpio$1 ]; then
		log "Exporting GPIO $1 with OUT direction"
		echo $1 > $GPIO_PATH/export
		echo "out" > $GPIO_PATH/gpio$1/direction
	fi
	if [ $2 = 1 ]; then
		if [ `cat $GPIO_PATH/gpio$1/value` = 1 ]; then
			echo 0 > $GPIO_PATH/gpio$1/value
			log "Switching GPIO $1 value"
		else
			log "Switching GPIO $1 value"
			echo 1 > $GPIO_PATH/gpio$1/value
		fi
	else
		log "Setting GPIO $1 to 0"
		echo 0 > $GPIO_PATH/gpio$1/value
	fi
}

while [ true ];
do
	pvmeta update gpio-test=pass
	for i in `ls $GPIO_META_PATH`;
	do
		set_gpio_value $i `cat $GPIO_META_PATH/$i`
	done
	sleep 1
done
