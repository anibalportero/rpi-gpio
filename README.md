# Toggle your RPi GPIOs via Pantavisor user-meta interface

Through Pantahub's user-meta tag for your device you can add
entries such as the following to dynamically set the values
on GPIO pins of the Pi header

```
{
  "gpio/9: "0",
  "gpio/11: "1"
}
```
